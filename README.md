# Tunning de BBDD - SQL Server 2019
Trabajo realizado entre 3 alumnos como parte de la asignatura **[42329	- Sistemas de Información Empresariales](https://guiae.uclm.es/vistaGuia/407/42329)** en el tercer curso de ingeniería informática, de la rama de ingeniería del software.    

En este trabajo, se nos entregó una BBDD de prueba con un peso de 50.279MB iniciales. Dicha BBDD, tras diversas acciones, disminuyó el tamaño en un 99,101%, pasando a ocupar 452MB frente a los 50.279MB iniciales. También, algunas consultas mejoraron en tiempo. Algunos casos de 15:22 minutos paso a 20 segundos.    

Finalmente, se eliminaron tablas y datos duplicados. Todo esto esta explicado en el pdf 'SIE2020_Memoria'.  

## Tecnologías Utilizadas
**Lenguaje de programación**: SQL  
**Herramientas de desarrollo**: SQL Server 2019 

Más información en los PDF's asociados.
